dataSource {
    pooled = true
    jmxExport = true
    driverClassName = "com.mysql.jdbc.Driver"
    dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
    dbCreate = "update"
    url = "jdbc:mysql://localhost/servitor?useUnicode=yes&characterEncoding=UTF-8"
    username = "root"
    password = "root"
}

hibernate {
    show_sql = false
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = "org.hibernate.cache.ehcache.EhCacheRegionFactory"
    //singleSession = true
}
